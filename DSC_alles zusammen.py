# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 14:31:02 2021

@author: Admin
"""
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 10:18:12 2021

@author: Admin
"""
import numpy as np
import pandas as pd
import os 

#%% load file from folder
os.chdir(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\04_Plausibiliserung\Testdaten von Messungen\210614_02_Sootloading_2h_2029')

                        
df_raw=pd.read_csv("210614_02_Sootloading_2h_2029.txt", sep="\t",encoding='latin-1',header=[0],dtype='O')

#delete Units
df_raw=df_raw.drop(labels=0, axis=0)

#change dytpe object to float
for column in df_raw:
    if df_raw[column].dtype == 'O':
        df_raw[column] = df_raw[column].astype(str).astype(float)
        
#%% calculate mean from DSC-test for all channels that are in standard names database

#   ----- Load variable table, where standard names are in. If field of raw data is a standard name, the mean value --------
#   ----- and the median value for each variable will be calculated and saved in calculated_values_df.              --------


#load variables to be checked
fields_to_be_checked = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\04_Plausibiliserung\01_Device & Sensor Check\Normnamen_fuer_Grenzwertüberwachung.xlsx',header=0)

variable_data_channel=[]
variable_data_mean=[]
variable_data_median=[]

calculated_values_df=pd.DataFrame(columns=['Channel','mean','median'])

#calculate values(mean and median) for each variable
counter=0
for column in df_raw:
    #for k in fields_to_be_checked:
    if column in fields_to_be_checked.values:
        variable_data_channel.append(column)
        variable_data_mean.append(df_raw[column].mean())
        variable_data_median.append(df_raw[column].median())
        counter=counter+1


calculated_values_df['Channel']=variable_data_channel
calculated_values_df['mean']=variable_data_mean
calculated_values_df['median']=variable_data_median

print("Es wurden",counter, "Channel für die Analyse vorbereitet.")

#%% limit check                 result in: errors_min_max

#   ----- Load comparison values for limit check from engine config file and compare values from calculated_values_df -----
#   ----- with the boundary values. If one of the values exceeds the limit it will be saved oder so ähnlich

limit_values = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\04_Plausibiliserung\01_Device & Sensor Check\limit_values_DSC_Iveco.xlsx')

# create df where limit deviations will be stored in
errors_min_max = pd.DataFrame(columns=['Channel','diff_value'])
errors_min_max['Channel'] = calculated_values_df['Channel']

# first "if"-block is to find right index for min & max-values of the channel. If values that is to be assessed is higher/lower 
# than min/max value, difference will be written in errors_min_max. + or - detects in which way it´s out of bounds.
for index in calculated_values_df.index:  
    for k in limit_values.index:
        if calculated_values_df.loc[index]['Channel'] == limit_values.loc[k]['Variable']:
            value_tba = calculated_values_df.loc[index]['mean'] #value to be assessed; mean value from whole channel data is used
            min_value = limit_values.loc[k]['Min']  
            max_value = limit_values.loc[k]['Max']
            if value_tba < min_value:
                errors_min_max.loc[index][1] = value_tba - min_value 
            if value_tba > max_value:
                errors_min_max.loc[index][1] = value_tba - max_value 

#%% channel compare(median)

#   ----- Calculate ambient temperature and pressure by taking median from all mean values from all temperature   -------
#   ----- sensors and pressure sensors                                                                            -------

# maxmimal deviation taken as 10 times of the value from one increment of the DAC + measurement uncertainty(see documentation)

T_dev = float(1.7) #allowed deviation to calculated ambient temeperature in '°C'
p_dev = float(31.5) #allowed deviation to calculated ambient temperature in 'mbar'

# defining df for value comparisen(median comparison) as df with all channels from calculated_values_df. errors will be 
#inserted at index of controlled channel

errors_val_cmp=pd.DataFrame(columns=['Channel','diff_value'])
errors_val_cmp['Channel'] = calculated_values_df['Channel']


############### TEMPERATURE ###############
 
#filter temperature values from all fields to be checked and collect them in one list where median is taken from 
# and taken as calculated ambient temperature

variable_data_channel_T=[]
variable_data_mean_T=[]

calculated_values_T = pd.DataFrame(columns=['Channel','mean'])

for i in calculated_values_df.index:
    if calculated_values_df['Channel'][i].startswith('T_'):                         #just used if it´s a temperature value
      #  if np.isnan(errors_min_max['diff_value'][i]) == True:                      #just used if limit_check didn´t already find error
            variable_data_channel_T.append(calculated_values_df['Channel'][i])
            variable_data_mean_T.append(calculated_values_df['mean'][i])
        
calculated_values_T['Channel']=variable_data_channel_T
calculated_values_T['mean']=variable_data_mean_T

calc_amb_T = calculated_values_T['mean'].median()

#   ------if mean value of channel is out of bounds compared to median over all temperatures + the deviation value, ------
#   ------an error will be written into errors_val_cmp for the certain channel. Otherwise the value is nan           ------


for i in calculated_values_T.index:
    if abs(calculated_values_T['mean'][i] - calc_amb_T) > T_dev:
        for index in calculated_values_df.index:
            if calculated_values_df['Channel'][index] == calculated_values_T['Channel'][i]:
                errors_val_cmp.loc[index][1]= calculated_values_T['mean'][i] - calc_amb_T
                

############### PRESSURE ###############

# filter pressure values from all fields to be checked and collect them in one list where median is taken from 
# and taken as calculated ambient pressure

variable_data_channel_p=[]
variable_data_mean_p=[]

calculated_values_p = pd.DataFrame(columns=['Channel','mean'])

for i in calculated_values_df.index:
    if calculated_values_df['Channel'][i].startswith('p_'):                           #just used if it´s a pressure value
    #    if np.isnan(errors_min_max['diff_value'][i]) == True:                        #just used if limit_check didn´t already find error
            if not bool(calculated_values_df['Channel'][i]=="p_fuel"):                #p_fuel is under pressure and therefore not ambient pressure                                  
                variable_data_channel_p.append(calculated_values_df['Channel'][i])
                variable_data_mean_p.append(calculated_values_df['mean'][i])
        
calculated_values_p['Channel']=variable_data_channel_p
calculated_values_p['mean']=variable_data_mean_p

calc_amb_p = calculated_values_p['mean'].median()

#   ------if mean value of channel is out of bounds compared to median over all pressures + the deviation value, ------
#   ------an error will be written into errors_val_cmp for the certain channel. Otherwise the value is nan           ------


for i in calculated_values_p.index:
    if abs(calculated_values_p['mean'][i] - calc_amb_p) > T_dev:
        for index in calculated_values_df.index:
            if calculated_values_df['Channel'][index] == calculated_values_p['Channel'][i]:
                errors_val_cmp.loc[index][1]= calculated_values_p['mean'][i] - calc_amb_p
                                                                               



        

