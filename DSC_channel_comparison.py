# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 15:50:15 2021

@author: Admin
"""
import numpy as np
import pandas as pd
import os 

#%%
os.chdir(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Testdaten von Messungen\210614_02_Sootloading_2h_2029')

                        
df_raw=pd.read_csv("210614_02_Sootloading_2h_2029.txt", sep="\t",encoding='latin-1',header=[0])

#delete Units
df_raw=df_raw.drop(labels=0, axis=0)

#change dytpe object to float
for column in df_raw:
    if df_raw[column].dtype == 'O':
        df_raw[column] = df_raw[column].astype(str).astype(float)
#%% 
#   ----- Load variable table, where standard names are in. If field of raw data is a standard name, the mean value --------
#   ----- and the median value for each variable will be calculated and saved in calculated_values_df.              --------


#load variables to be checked
fields_to_be_checked = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Kapitel Plausibilisierung inkl. SdT\Normnamen_fuer_Grenzwertüberwachung.xlsx',header=0)

variable_data_channel=[]
variable_data_mean=[]
variable_data_median=[]

calculated_values_df=pd.DataFrame(columns=['Channel','mean','median'])

#calculate values(mean and median) for each variable
counter=0
for column in df_raw:
    #for k in fields_to_be_checked:
    if column in fields_to_be_checked.values:
        variable_data_channel.append(column)
        variable_data_mean.append(df_raw[column].mean())
        variable_data_median.append(df_raw[column].median())
        counter=counter+1


calculated_values_df['Channel']=variable_data_channel
calculated_values_df['mean']=variable_data_mean
calculated_values_df['median']=variable_data_median

print("Es wurden",counter, "Channel für die Analyse vorbereitet.")

#%%
#   ----- Calculate ambient temperature and pressure by taking median from all mean values from all temperature   -------
#   ----- sensors and pressure sensors                                                                            -------

from DSC_limit_check.py import errors_min_max
# maxmimal deviation taken as 10 times of the value from one increment of the DAC + measurement uncertainty(see documentation)

T_dev = float(1.7) #allowed deviation to calculated ambient temeperature in '°C'
p_dev = float(31.5) #allowed deviation to calculated ambient temperature in 'mbar'

# TEMPERATURE
 
#filter temperature values from all fields to be checked and collect them in one list where median is taken from 
# and taken as calculated ambient temperature

variable_data_channel_T=[]
variable_data_mean_T=[]

calculated_values_T = pd.DataFrame(columns=['Channel','mean'])

for i in calculated_values_df.index:
    if calculated_values_df['Channel'][i].startswith('T_'):
        if np.isnan(errors_min_max['diff_value'][i]) == True:    #just used if limit_check didn´t already find error
            variable_data_channel_T.append(calculated_values_df['Channel'][i])
            variable_data_mean_T.append(calculated_values_df['mean'][i])
        
calculated_values_T['Channel']=variable_data_channel_T
calculated_values_T['mean']=variable_data_mean_T

calc_amb_T = calculated_values_T['mean'].median()

# PRESSURE

#filter pressure values from all fields to be checked and collect them in one list where median is taken from 
# and taken as calculated ambient pressure

variable_data_channel_p=[]
variable_data_mean_p=[]

calculated_values_p = pd.DataFrame(columns=['Channel','mean'])

for i in calculated_values_df.index:
    if calculated_values_df['Channel'][i].startswith('p_'):     #just used if limit_check didn´t already find error
        if np.isnan(errors_min_max['diff_value'][i]) == True:
            variable_data_channel_T.append(calculated_values_df['Channel'][i])
            variable_data_mean_T.append(calculated_values_df['mean'][i])
        
calculated_values_p['Channel']=variable_data_channel_T
calculated_values_p['mean']=variable_data_mean_T

calc_amb_p = calculated_values_p['mean'].median()
