# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 10:18:12 2021

@author: Admin
"""
import numpy as np
import pandas as pd
import os 

#%%
os.chdir(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Testdaten von Messungen\210614_02_Sootloading_2h_2029')

                        
df_raw=pd.read_csv("210614_02_Sootloading_2h_2029.txt", sep="\t",encoding='latin-1',header=[0])

#delete Units
df_raw=df_raw.drop(labels=0, axis=0)

#change dytpe object to float
for column in df_raw:
    if df_raw[column].dtype == 'O':
        df_raw[column] = df_raw[column].astype(str).astype(float)
#%% 
#   ----- Load variable table, where standard names are in. If field of raw data is a standard name, the mean value --------
#   ----- and the median value for each variable will be calculated and saved in calculated_values_df.              --------


#load variables to be checked
fields_to_be_checked = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Kapitel Plausibilisierung inkl. SdT\Normnamen_fuer_Grenzwertüberwachung.xlsx',header=0)

variable_data_channel=[]
variable_data_mean=[]
variable_data_median=[]

calculated_values_df=pd.DataFrame(columns=['Channel','mean','median'])

#calculate values(mean and median) for each variable
for column in df_raw:
    #for k in fields_to_be_checked:
    if column in fields_to_be_checked.values:
        variable_data_channel.append(column)
        variable_data_mean.append(df_raw[column].mean())
        variable_data_median.append(df_raw[column].median())

calculated_values_df['Channel']=variable_data_channel
calculated_values_df['mean']=variable_data_mean
calculated_values_df['median']=variable_data_median

#%%
#   ----- Load comparison values for limit check from engine config file and compare values from calculated_values_df -----
#   ----- with the boundary values. If one of the values exceeds the limit it will be saved oder so ähnlich

limit_values = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Kapitel Plausibilisierung inkl. SdT\limit_values_DSC_Iveco.xlsx')

#create df where limit deviations will be stored in
errors_min_max = pd.DataFrame(columns=['Channel','diff_value'])
errors_min_max['Channel'] = calculated_values_df['Channel']

# first "if"-block is to find right index for min & max-values of the channel. If values that is to be assessed is higher/lower 
# than min/max value, difference will be written in errors_min_max. + or - detects in which way it´s out of bounds.
for index in calculated_values_df.index:  
    for k in limit_values.index:
        if calculated_values_df.loc[index]['Channel'] == limit_values.loc[k]['Variable']:
            value_tba = calculated_values_df.loc[index]['mean'] #value to be assessed; mean value from whole channel data is used
            min_value = limit_values.loc[k]['Min']  
            max_value = limit_values.loc[k]['Max']
            if value_tba < min_value:
                errors_min_max.loc[index][1] = value_tba - min_value 
            if value_tba > max_value:
                errors_min_max.loc[index][1] = value_tba - max_value 
                                                                               



