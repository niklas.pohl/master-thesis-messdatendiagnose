# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 11:58:35 2021

@author: Admin
"""
import pandas as pd
import os
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt

#%% load file from folder
testname = '210615_03_Regeneration_50min_600_2038'
os.chdir(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\04_Plausibiliserung\Testdaten von Messungen\210614_07_Regeneration_50min_2034')

                        
df_raw=pd.read_csv("210614_07_Regeneration_50min_2034.txt", sep="\t",encoding='latin-1',header=[0],dtype='O')

#delete Units
df_raw = df_raw.drop(labels=0, axis=0)
df_raw = df_raw.reset_index()
#%%
#change dytpe object to float
for column in df_raw:
    if df_raw[column].dtype == 'O':
        df_raw[column] = df_raw[column].astype(str).astype(float)
        
del column


#%% Correction of time delay of exhaust gas measurements

#calculate the correlation coefficient for each value in the compared columns
def finddelay(data1,data2):
    #https://stackoverflow.com/questions/62987317/the-normalized-cross-correlation-of-two-signals-in-python
    """
    function returns calculated delay between the two inserted 1D datasets.
    Datasets need to be the same length.
    data1 is the column that is referred to.
    """
    error=""
    delay = np.nan
    if data1.isnull().all() or data2.isnull().all() == False:    #checking if there are values in the column 
        correlation = signal.correlate(data1 ,data2 ,mode="same")          #compute the correlation over whole dataset
        delay= np.argmax(correlation)-int(len(correlation)/2)              #find index where correlation is max and compute delay with that 
    else:
        error = str("one of the two inserted data packages is empty")
    return delay,error

#correlation = signal.correlate(df_raw['Exh_MF_INCA'] ,df_raw['NOx_L1'] ,mode="same")
#delay= np.argmax(correlation)-int(len(correlation)/2)

tau = {}
                   
tau['tau_NOx_L1'] = finddelay(df_raw['Exh_MF_INCA'],df_raw['NOx_L1'])    
tau['tau_NOx_L2'] = finddelay(df_raw['Exh_MF_INCA'],df_raw['NOx_L2'])
tau['tau_NOx_L3'] = finddelay(df_raw['Exh_MF_INCA'],df_raw['NOx_L3'])
tau['tau_NO_L1'] = finddelay(df_raw['Exh_MF_INCA'],df_raw['NO_L1'])
tau['tau_NO_L2'] = finddelay(df_raw['Exh_MF_INCA'],df_raw['NO_L2'])
tau['tau_NO_L3'] = finddelay(df_raw['Exh_MF_INCA'],df_raw['NO_L3'])   


#%% 
# ------ use timecorrection on exhaust gas columns and insert them in a new df df_Exh_corr -------

def timecorrection(tau,data):
    data_corr = []              #define empty list where corrected values from point of delay are put in
    if tau < 0:
        for j in range(len(data)+tau):
            data_corr.append((data[j-tau]));    #append data from point of delay to be aligned with Exh_mf_ECU
        for k in range(abs(tau)):
            data_corr.append(data.iloc[-1]);        # append last value of data at the end to fill up missing values
    #Exh_tcorr = df_raw.assign(**{(data.name + "_corr") : data_corr}) #assign new columns to new df with corrected data
    return data_corr

Exh_proc = {}
Exh_proc["NOx_L1_tcorr"] = np.array(timecorrection(tau['tau_NOx_L1'][0],df_raw['NOx_L1']))
Exh_proc['NOx_L2_tcorr'] = np.array(timecorrection(tau['tau_NOx_L2'][0],df_raw['NOx_L2']))
Exh_proc['NOx_L3_tcorr'] = np.array(timecorrection(tau['tau_NOx_L3'][0],df_raw['NOx_L3']))
Exh_proc['NO_L1_tcorr'] = np.array(timecorrection(tau['tau_NO_L1'][0],df_raw['NO_L1']))
Exh_proc['NO_L2_tcorr'] = np.array(timecorrection(tau['tau_NO_L2'][0],df_raw['NO_L2']))
Exh_proc['NO_L3_tcorr'] = np.array(timecorrection(tau['tau_NO_L3'][0],df_raw['NO_L3']))

#df_Exh_corr = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in Exh_corr.items() ]))  #https://stackoverflow.com/questions/19736080/creating-dataframe-from-a-dictionary-where-entries-have-different-lengths

"""
    if delay < 0:
        for j in range(len(df_raw['NOx_L1'])+delay):
            NOx_L1_corr.append((df_raw['NOx_L1'][j-delay]));
        for k in range(abs(delay)):
            NOx_L1_corr.append(df_raw['NOx_L1'].iloc[-1]);
    Exh_tcorr = df_raw.assign(NOx_L1_corr = NOx_L1_corr)
    
    fig, ax = plt.subplots()
    df_raw2.plot(x = 'recorder_time', y = 'Exh_MF_INCA', ax = ax)
    df_raw2.plot(x = 'recorder_time', y = 'NOx_corr', ax = ax,secondary_y = True)
"""
#%%
#fig, ax = plt.subplots()
#NOx_L2_corr.plot(x = 'recorder_time', y = 'NOx_L2_corr', ax = ax)
#df_raw.plot(x = 'recorder_time', y = 'NOx_L2', ax = ax, secondary_y = True)

#%% Calculation dry to wet exhaust gas
def dry_wet_corr_factor (relHum,p_0,T_0,air_mf,fuel_mf):
    Cv = []
    for index in df_raw.index:
        R_a = relHum[index]    #relative Humidity of intake air [%] SN=Box_relHum_ECU
        p_B = p_0[index]/10              # /10 because of calculation from mbar to kPa Ambient pressure [kPa]
        t_0 = T_0[index]               #Ambient temperature [°C]
        p_a = 0.61115*10**((7.602*t_0)/(241.2+t_0)) #Saturation vapor pressure of intake air [kPa]
       
        if  air_mf.loc[index] == 0: #df_raw['Fuel_MF_INCA'].loc[index] and
            Cv.append(float(1))
        else:   
            F_fh = 1.969/(1+(fuel_mf.loc[index]/air_mf.loc[index]))
            H_a = (6.22*R_a*p_a)/(p_B-p_a*R_a*10**(-2)) #g water per kg dry air
            K_w2 = (1.608*H_a)/(1000+(1.608*H_a))
            Cv.append((1 - F_fh*(fuel_mf.loc[index]/air_mf.loc[index])-K_w2)) #Corrective value for calculation from dry to wet exhaust gas
    return Cv

Exh_proc ['Cv'] = np.array(dry_wet_corr_factor(df_raw['Luftfeuchtigkeit_INCA'],df_raw['p_0'],df_raw['T_0'],df_raw['HFM_INCA'],df_raw['Fuel_MF_INCA']))

def calc_wet (Cv,Exh_gas):
    if Exh_gas .size == 0:
        Exh_gas_wet = np.empty(0)
    else:
        Exh_gas_wet = Cv*Exh_gas
    return Exh_gas_wet

"""
Exhaust gases Processed is defined as:
    1. Allocation to measuring point
    2. time corrected
    3. calcuation from dry to wet exhaust gas is measured dry 
the original data is still to be found in df_raw
"""
Exh_proc ['AMA_NOx_L1_proc'] = calc_wet(Exh_proc['Cv'],Exh_proc["NOx_L1_tcorr"])
Exh_proc ['AMA_NOx_L2_proc'] = calc_wet(Exh_proc['Cv'],Exh_proc["NOx_L2_tcorr"])
Exh_proc ['AMA_NOx_L3_proc'] = calc_wet(Exh_proc['Cv'],Exh_proc["NOx_L3_tcorr"])
Exh_proc ['AMA_NO_L1_proc'] = calc_wet(Exh_proc['Cv'],Exh_proc["NO_L1_tcorr"])
Exh_proc ['AMA_NO_L2_proc'] = calc_wet(Exh_proc['Cv'],Exh_proc["NO_L2_tcorr"])
Exh_proc ['AMA_NO_L3_proc'] = calc_wet(Exh_proc['Cv'],Exh_proc["NO_L3_tcorr"])


df_Exh_proc = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in Exh_proc.items() ]))  #https://stackoverflow.com/questions/19736080/creating-dataframe-from-a-dictionary-where-entries-have-different-lengths

#Cv = []
#for index in df_raw.index:
#    R_a = df_raw['Luftfeuchtigkeit_INCA'][index]    #relative Humidity of intake air [%] SN=Box_relHum_ECU
#    p_B = df_raw['p_0'][index]/10              # /10 because of calculation from mbar to kPa Ambient pressure [kPa]
#    t_0 = df_raw['T_0'][index]               #Ambient temperature [°C]
#    p_a = 0.61115*10**((7.602*t_0)/(241.2+t_0)) #Saturation vapor pressure of intake air [kPa]
   
#    if  df_raw['HFM_INCA'].loc[index] == 0: #df_raw['Fuel_MF_INCA'].loc[index] and
#        Cv.append(float(1))
#    else:   
#        F_fh = 1.969/(1+(df_raw['Fuel_MF_INCA'].loc[index]/df_raw['HFM_INCA'].loc[index]))
#        H_a = (6.22*R_a*p_a)/(p_B-p_a*R_a*10**(-2)) #g water per kg dry air
#        K_w2 = (1.608*H_a)/(1000+(1.608*H_a))
 #       Cv.append((1 - F_fh*(df_raw['Fuel_MF_INCA'].loc[index]/df_raw['HFM_INCA'].loc[index])-K_w2)) #Corrective value for calculation from dry to wet exhaust gas
    
#fig, ax = plt.subplots()
#NOx_L2_corr.plot(x = 'recorder_time', y = 'NOx_L2_corr', ax = ax)
#df_raw.plot(x = 'recorder_time', y = 'NOx_L2', ax = ax, secondary_y = True)

#%% load constants
constants = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\04_Plausibiliserung\00_Konfig\Konfig_constants.xlsx')
 
const = dict(zip(constants['Parameter'],constants['Wert']))
#%% calculation of cumulative and specific values from raw data

#initialize dicts for calculated data(same length as raw) and specific data(just one value)
data_calc = {}
data_spec = {}

# ------- recorder_time into time_s and sample rate ------

data_spec['sample_rate_ms'] = df_raw['recorder_time'][1]-df_raw['recorder_time'][0]
data_spec['sample_rate_s'] = data_spec['sample_rate_ms']/1000
data_calc['time_s'] = np.array(df_raw['recorder_time']-df_raw['recorder_time'][0])

# ------- exhaust gas values -------
# NO2 Calculation
data_calc['AMA_NO2_L1_proc'] = df_Exh_proc['AMA_NOx_L1_proc'] - df_Exh_proc['AMA_NO_L1_proc']
data_calc['AMA_NO2_L2_proc'] = df_Exh_proc['AMA_NOx_L2_proc'] - df_Exh_proc['AMA_NO_L2_proc']
data_calc['AMA_NO2_L3_proc'] = df_Exh_proc['AMA_NOx_L3_proc'] - df_Exh_proc['AMA_NO_L3_proc']

def exhgas_ppm_to_gs(rho_gas,Exh_mf,exhgas_ppm):
    exhgas_gs = (10**(-6)/3.6)*(rho_gas/const['rho_Exh'])*Exh_mf*exhgas_ppm
    return exhgas_gs

data_calc['AMA_NOx_L1_proc_gs'] = exhgas_ppm_to_gs(const['rho_NOx'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NOx_L1_proc'])
data_calc['AMA_NOx_L2_proc_gs'] = exhgas_ppm_to_gs(const['rho_NOx'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NOx_L2_proc'])
data_calc['AMA_NOx_L3_proc_gs'] = exhgas_ppm_to_gs(const['rho_NOx'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NOx_L2_proc'])

data_calc['AMA_NO_L1_proc_gs'] = exhgas_ppm_to_gs(const['rho_NO'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NO_L1_proc'])
data_calc['AMA_NO_L2_proc_gs'] = exhgas_ppm_to_gs(const['rho_NO'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NO_L2_proc'])
data_calc['AMA_NO_L3_proc_gs'] = exhgas_ppm_to_gs(const['rho_NO'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NO_L3_proc'])

#data_calc['AMA_NO2_L1_proc_gs'] = exhgas_ppm_to_gs(const['rho_NO2'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NO2_L1_proc'])
#data_calc['AMA_NO2_L2_proc_gs'] = exhgas_ppm_to_gs(const['rho_NO2'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NO2_L2_proc'])
#data_calc['AMA_NO2_L3_proc_gs'] = exhgas_ppm_to_gs(const['rho_NO2'],df_raw['Exh_MF_INCA'],df_Exh_proc['AMA_NO2_L3_proc'])

#!!!!!!!!!!!!!!!!!!!auch noch für andere Abgase machen!!!!!!!!!!!!!!!!!!



#%%
data_calc['PF_p_diff'] = df_raw['p_7']-df_raw['p_8']
data_calc['Exh_vf_cm3s'] = (10**6/3600)*df_raw['Exh_MF_INCA']/const['rho_Exh']



#%% Calculation Lambda (Brettschneider)

### Vereinfachungen nach Flohr Funktionsbeschreibung S.68

R_a = rel_Hum_ECU
HCr = (2*h_fuel*M_C)/(c_fuel*M_H)               # ratio of hydrogen to carbon in fuel
OCr = (2*o_fuel*M_C)/(c_fuel*M_O)               # ratio of oxygen to carbon in fuel

