# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 13:44:30 2021

@author: Admin
"""
import os
import pandas as pd

##Loading of the file to be processed

os.chdir(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Testdaten von Messungen\210614_02_Sootloading_2h_2029')

                        
df_raw=pd.read_csv("210614_02_Sootloading_2h_2029.txt", sep="\t",encoding='latin-1',header=[0])

#delete Units
df_raw=df_raw.drop(labels=0, axis=0)

#change dytpe object to float (all fields have the datatype float now)
for column in df_raw:
    if df_raw[column].dtype == 'O':
        df_raw[column] = df_raw[column].astype(str).astype(float)
        
#load variables to be checked
fields_to_be_checked = pd.read_excel(r'C:\Users\Admin\HESSENBOX-DA\Master-Thesis DB & KI\Kapitel Plausibilisierung inkl. SdT\Normnamen_fuer_Grenzwertüberwachung.xlsx',header=0)



###########Umrechnung aller Werte auf genormte Einheiten 